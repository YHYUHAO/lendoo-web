'use strict';
var router = require('express').Router();
var AV = require('leanengine');

router.get('/login', function(req, res, next) {
	// 如果已经登录，自动跳转
	if (req.currentUser) {
        res.redirect('/goods/list');
    }
	res.render('manager/login', {
		layout: 'sub'
	});
});

router.post('/login', function (req, res, next) {
	var user = req.body;
	AV.User.logIn(user.username, user.password).then(user => {
		console.log(user);
		res.saveCurrentUser(user);
		res.send(user);
	}, err => {
		console.log(err);
		res.send(err);
	});
});

router.get('/logout', function (req, res, next) {
	req.currentUser.logOut();
	res.clearCurrentUser(); // 从 Cookie 中删除用户
	res.redirect('/manager/login');
});

module.exports = router;